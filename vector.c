#include "vector.h"

void Vec2UnpackTexCoords(int in, vec2_t *uv)
{
    register __m128i packed = _mm_cvtsi32_si128(in);
    register __m128 unpacked = _mm_cvtph_ps(packed);

    _mm_storel_pi(&uv->uv, unpacked);
}