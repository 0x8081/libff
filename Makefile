TARGET = libff.so
SRCDIR = .
CC = gcc
CXX = g++
CFLAGS = -O3 -std=c17 -fPIC -march=znver1 -mtune=znver1
CXXFLAGS = -O3 -std=c++17 -fPIC -march=znver1 -mtune=znver1
LDFLAGS = -lz
CSRCS = $(wildcard *.c)
CXXSRCS = $(wildcard *.cpp)
OBJS = $(CSRCS:.c=.o) $(CXXSRCS:.cpp=.o)

%.o: %.c ; $(CC) -c $(CFLAGS) $<
%.o: %.cpp ; $(CXX) -c $(CXXFLAGS) $<

release: $(TARGET)

$(TARGET): $(OBJS)
	$(CC) $^ $(CFLAGS) $(LDFLAGS) -shared -o $@

clean:
	rm $(TARGET) $(OBJS)