#pragma once

#include "common.h"

typedef struct DB_Header
{
	char magic[8];
	u32 version;
} DB_Header;

typedef struct DB_AuthSignature
{
	char bytes[256];
} DB_AuthSignature;

typedef struct DB_AuthHeader
{
	char magic[8];
	u32 reserved;
	char fastfileName[32];
	DB_AuthSignature singedHash;
} DB_AuthHeader;

typedef struct DB_Section
{
	u32 size;
	u8 *data;
} DB_Section;

typedef struct FastFile
{
	DB_Header dbHdr;
	DB_AuthHeader dbAuthHdr;
	DB_Section *sections;
} FastFile;

EXPORT void *FastFile_Alloc(FILE *f, size_t *size);
EXPORT void FastFile_Free(void *buf, DB_Section *sections, u32 count);
EXPORT void FastFile_LoadSections(void *buf, size_t size, FastFile *ff, u32 *count);
EXPORT void FastFile_GetHeader(void *buf, FastFile *hdr);
EXPORT void *FastFile_Decrypt(void *buf, size_t *size, DB_Section *sections, u32 count);