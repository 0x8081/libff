#include "salsa.h"

#include "salsa20.h"

#define IV_TABLE_SIZE 16000

#ifdef __cplusplus
extern "C"
{
#endif

#include "sha1.h"

	void s20_ivTableInit(u8 *ivTable, u8 *nameKey)
	{
		int len = strlen((char *)nameKey);

		int div = 0;
		for (int i = 0; i < IV_TABLE_SIZE; i += (len * 4))
		{
			for (int x = 0; x < (len * 4); x += 4)
			{
				if ((i + div) >= IV_TABLE_SIZE || i + x >= IV_TABLE_SIZE)
					return;

				if (x > 0)
					div = x / 4;
				else
					div = 0;

				for (int y = 0; y < 4; y++)
					ivTable[i + x + y] = nameKey[div];
			}
		}
	}

	void s20_ivTableUpdate(u32 index, u8 *digest, u8 *ivTable, u32 *ivCounter)
	{
		for (int i = 0; i < 20; i += 5)
		{
			int val = (index + 4 * ivCounter[index]) % 800 * 5;
			for (int x = 0; x < 5; x++)
				ivTable[4 * val + x + i] ^= digest[i + x];
		}

		ivCounter[index]++;
	}

	void s20_getIv(u8 *iv, u32 index, u8 *ivTable, u32 *ivCounter)
	{
		int i = (index + 4 * (ivCounter[index] - 1)) % 800 * 20;
		memcpy(iv, &ivTable[i], 8);
	}

	void s20_crypt(u8 *key, u8 *nameKey, DB_Section *sections, u32 count)
	{
		u8 digest[32];
		u8 *ivTable = (u8 *)calloc(1, IV_TABLE_SIZE);
		u32 ivCounter[4] = {1, 1, 1, 1};
		u8 iv[8] = {0, 0, 0, 0, 0, 0, 0, 0};

		ucstk::Salsa20 s20 = ucstk::Salsa20(key);

		s20_ivTableInit(ivTable, nameKey);

		for (u32 i = 0; i < count; i++)
		{
			s20_getIv(iv, i % 4, ivTable, ivCounter);

			void *decrypted_data = calloc(1, sections[i].size);
			if (!decrypted_data)
			{
				free(ivTable);
				return;
			}

			s20.setIv(iv);
			s20.processBytes(sections[i].data, (u8 *)decrypted_data, sections[i].size);

			memcpy(sections[i].data, decrypted_data, sections[i].size);
			SHA1((char *)digest, (const char *)sections[i].data, sections[i].size);

			s20_ivTableUpdate(i % 4, digest, ivTable, ivCounter);

			free(decrypted_data);
		}

		free(ivTable);
	}

#ifdef __cplusplus
}
#endif