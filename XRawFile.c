#include "XRawFile.h"

void RawFile_Read(RawFile *rawfile, void *buf)
{
    size_t name_len = 0;
    if (!buf)
        return;

    rawfile = calloc(1, sizeof(RawFile));

    rawfile->xRawFile = calloc(1, sizeof(XRawFile));
    memcpy(rawfile->xRawFile, buf, sizeof(XRawFile));

    rawfile->nameLen = strlen((const char *)(buf) + sizeof(XRawFile)) + 1;
    rawfile->name = calloc(1, rawfile->nameLen);
    memcpy(rawfile->name, (buf + sizeof(XRawFile)), rawfile->nameLen);

    rawfile->buf = calloc(1, rawfile->xRawFile->size);
    memcpy(rawfile->buf, (buf + sizeof(XRawFile) + rawfile->nameLen), rawfile->xRawFile->size);
}

void RawFile_Free(RawFile *rawfile)
{
    free(rawfile->xRawFile);
    free(rawfile->name);
    free(rawfile->buf);
    free(rawfile);
}