#pragma once

#include "common.h"

typedef struct _XRawFile
{
    u32 namePtr;
    u32 size;
    u32 buffPtr;
} XRawFile;

typedef struct _RawFile
{
    XRawFile *xRawFile;
    char *name;
    size_t nameLen;
    u8 *buf;
} RawFile;