#pragma once

#include "common.h"

#include "FastFile.h"

#ifdef __cplusplus
extern "C"
{
#endif

    void s20_crypt(u8 *key, u8 *nameKey, DB_Section *sections, u32 count);

#ifdef __cplusplus
}
#endif