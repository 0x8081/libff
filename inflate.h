#pragma once

#include "common.h"

#define CHUNK 128 * 1024

int inf(u8 *src, size_t srclen, u8 *dst, size_t *dstlen);
void zerr(int ret);