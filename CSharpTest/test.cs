using System;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;

using BO2;

class Test
{
    public static void Main(string[] args)
    {
        if (args.Length < 1)
            return;

        if (!File.Exists(args[0]))
            return;

        Console.WriteLine("Opening: " + args[0]);

        FastFile ff = new FastFile(args[0]);

        Console.WriteLine("FF Decompressed Size: " + ff.decompressed_size);
    }
}