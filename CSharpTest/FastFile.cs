using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;

namespace BO2
{
    class FastFile
    {
#if WIN64
        const string stdclib = "msvcrt.dll";
        const string libff = "libff.dll";
#else
        const string stdclib = "libc.so.6";
        const string libff = "libff.so";
#endif

        // Standard C Lib Imports

        [DllImport(stdclib)] private static extern IntPtr fopen(String filename, String mode);
        [DllImport(stdclib)] private static extern UInt32 fclose(IntPtr file);
        [DllImport(stdclib)] private static extern void free(IntPtr ptr);

        // LibFF Imports

        [DllImport(libff)] private static extern IntPtr FastFile_Alloc(IntPtr f, ref UInt64 size);
        [DllImport(libff)] private static extern void FastFile_Free(IntPtr buf, IntPtr sections, UInt32 count);
        [DllImport(libff)] private static extern void FastFile_GetHeader(IntPtr buf, ref FastFileHeader hdr);
        [DllImport(libff)] private static extern void FastFile_LoadSections(IntPtr buf, UInt64 size, ref FastFileHeader hdr, ref UInt32 count);
        [DllImport(libff)] private static extern IntPtr FastFile_Decrypt(IntPtr buf, ref UInt64 size, IntPtr sections, UInt32 count);

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        struct DB_Header
        {
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 8)]
            string magic;

            [MarshalAs(UnmanagedType.U4)]
            UInt32 version;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct DB_AuthHeader
        {
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 8)]
            string magic;

            [MarshalAs(UnmanagedType.U4)]
            UInt32 reserved;

            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)]
            public string fastfileName;

            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 1 * 256)]
            byte[] signedHash;
        }

        [StructLayout(LayoutKind.Sequential)]
        struct DB_Section
        {
            [MarshalAs(UnmanagedType.U4)]
            public UInt32 size;

            [MarshalAs(UnmanagedType.U8)]
            public IntPtr data;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct FastFileHeader
        {
            DB_Header dbHdr;
            public DB_AuthHeader dbAuthHdr;
            public IntPtr sections;
        }

        FastFileHeader hdr = new FastFileHeader();

        IntPtr buf;
        IntPtr decompressed_buf;
        UInt32 sectionCount;
        public UInt64 size;
        public UInt64 decompressed_size;

        public FastFile(string path)
        {
            if (!File.Exists(path))
            {
                throw new Exception("File does not exist");
            }

            IntPtr f = fopen(path, "rb");
            buf = FastFile_Alloc(f, ref size);
            fclose(f);

            FastFile_GetHeader(buf, ref hdr);
            FastFile_LoadSections(buf, size, ref hdr, ref sectionCount);
            decompressed_buf = FastFile_Decrypt(buf, ref decompressed_size, hdr.sections, sectionCount);
        }

        ~FastFile()
        {
            free(decompressed_buf);
        }
    }
}
