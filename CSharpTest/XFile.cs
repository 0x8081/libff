using System;
using System.Runtime.InteropServices;

namespace BO2
{
    class XFile
    {
        public enum XFileType
        {
            XFILE_BLOCK_TEMP = 0,
            XFILE_BLOCK_RUNTIME_VIRTUAL = 1,
            XFILE_BLOCK_RUNTIME_PHYSICAL = 2,
            XFILE_BLOCK_DELAY_VIRTUAL = 3,
            XFILE_BLOCK_DELAY_PHYSICAL = 4,
            XFILE_BLOCK_VIRTUAL = 5,
            XFILE_BLOCK_PHYSICAL = 6,
            XFILE_BLOCK_STREAMER_RESERVE = 7,
            MAX_XFILE_COUNT
        };

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct XFileHeader
        {
            [MarshalAs(UnmanagedType.U4)]
            public UInt32 size;

            [MarshalAs(UnmanagedType.U4)]
            public UInt32 externalSize;

            [MarshalAs(UnmanagedType.U4)]
            public UInt32 blockSize;
        };

        XFileHeader hdr;
    }
}