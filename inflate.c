#include "inflate.h"

#include <zlib.h>

int inf(u8 *src, size_t srclen, u8 *dst, size_t *dstlen)
{
  int ret = 0;

  z_stream strm;
  strm.zalloc = Z_NULL;
  strm.zfree = Z_NULL;
  strm.opaque = Z_NULL;
  strm.avail_in = (u32)srclen;
  strm.next_in = src;
  strm.avail_out = CHUNK;
  strm.next_out = dst;

  ret = inflateInit2(&strm, -MAX_WBITS);
  if (ret != Z_OK)
      return ret;

  while (strm.total_in != srclen) {
      ret = inflate(&strm, Z_NO_FLUSH);
      if (ret != Z_STREAM_END && ret != Z_OK)
          return ret;
  }

  *dstlen = strm.total_out;

  inflateEnd(&strm);

  return 0;
}

void zerr(int ret)
{
  fputs("zpipe: ", stderr);
  switch (ret)
  {
  case Z_ERRNO:
    if (ferror(stdin))
      fputs("error reading stdin\n", stderr);
    if (ferror(stdout))
      fputs("error writing stdout\n", stderr);
    break;
  case Z_STREAM_ERROR:
    fputs("invalid compression level\n", stderr);
    break;
  case Z_DATA_ERROR:
    fputs("invalid or incomplete deflate data\n", stderr);
    break;
  case Z_MEM_ERROR:
    fputs("out of memory\n", stderr);
    break;
  case Z_VERSION_ERROR:
    fputs("zlib version mismatch!\n", stderr);
  }
}