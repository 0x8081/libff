#pragma warning(disable : 4996)

#include "FastFile.h"

#include "inflate.h"
#include "keys.h"
#include "salsa.h"

#define MAX_SECTIONS 16384

// Allocates and Loads FastFile into Memory
void *FastFile_Alloc(FILE *f, size_t *size)
{
	void *buf = NULL;

	fseek(f, 0, SEEK_END);
	*size = ftell(f);
	fseek(f, 0, SEEK_SET);

	buf = calloc(1, *size);
	if (!buf)
		return NULL;

	fread(buf, *size, 1, f);

	return buf;
}

void FastFile_Free(void *buf, DB_Section *sections, u32 count)
{
	if (count != 0 || sections != NULL)
	{
		for (u32 i = 0; i < count; i++)
		{
			free(sections[i].data);
			sections[i].size = 0;
			sections[i].data = NULL;
		}
	}

	free(sections);
	free(buf);
};

void FastFile_LoadSections(void *buf, size_t size, FastFile *ff, u32 *count)
{
	void *section_list = NULL;
	void *section_data = NULL;
	u32 section_size = 0;
	size_t pos = 0;

	section_list = calloc(1, sizeof(DB_Section) * MAX_SECTIONS);
	if (!section_list)
	{
		ff->sections = NULL;
		*count = 0;
		return;
	}

	*count = 1;
	ff->sections = section_list;
	pos = (size_t)buf + (sizeof(FastFile) - sizeof(DB_Section **));

	while ((pos - (size_t)buf) < size)
	{
		section_size = *(u32 *)pos;
		if (section_size == 0)
		{
			*count -= 1;
			break;
		}

		section_data = calloc(1, section_size);
		if (!section_data)
			break;
		memcpy(section_data, (void *)(pos + 0x4), section_size);

		ff->sections[*count - 1].size = section_size;
		ff->sections[*count - 1].data = (u8 *)section_data;

		*count = *count + 1;
		pos += ((size_t)section_size + 0x4);
	}
}

void FastFile_GetHeader(void *buf, FastFile *hdr)
{
	memset(hdr, 0, sizeof(FastFile));
	memcpy(hdr, buf, sizeof(FastFile) - sizeof(DB_Section *));
}

void *FastFile_Decrypt(void *buf, size_t *size, DB_Section *sections, u32 count)
{
	void *decompressed_buf;
	size_t total_decompressed_size = 0;
	u8 nameKey[32];
	memcpy(nameKey, (u8 *)(buf) + 0x18, 32);

	s20_crypt(Key_BO2_PC, nameKey, sections, count);

	for (u32 i = 0; i < count; i++)
	{
		int ret = 0;
		size_t decompressed_size = 0;
		decompressed_buf = calloc(1, CHUNK);

		if (!decompressed_buf)
		{
			sections[i].data = NULL;
			sections[i].size = 0;
			return NULL;
		}

		ret = inf(sections[i].data, sections[i].size, decompressed_buf, &decompressed_size);

		if (ret != 0 && ret != 1)
		{
			zerr(ret);
			free(decompressed_buf);
			return NULL;
		}

		free(sections[i].data);

		sections[i].size = (u32)decompressed_size;
		sections[i].data = calloc(1, decompressed_size);
		if (!sections[i].data)
		{
			sections[i].size = 0;
			free(decompressed_buf);
			return NULL;
		}

		total_decompressed_size += decompressed_size;

		memcpy(sections[i].data, decompressed_buf, decompressed_size);
		free(decompressed_buf);
	}

	decompressed_buf = calloc(1, total_decompressed_size);

	for (int i = 0; i < count; i++)
	{
		if (i == 0)
		{
			memcpy(decompressed_buf, sections[i].data, sections[i].size);
			continue;
		}
		else
		{
			memcpy(decompressed_buf + sections[i - 1].size, sections[i].data, sections[i].size);
		}
	}

	*size = total_decompressed_size;

	return decompressed_buf;
}
