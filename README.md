# libFF

C Library for reading FastFile data archives (WIP)

Currently only supports Black Ops 2 PC

### Credit to Everad for the Salsa20 C++ library
[here](https://github.com/everard/Salsa20)

### Credit to Steve Reid for SHA-1 C code
[here](https://github.com/clibs/sha1)