#pragma warning(disable : 4996)

#include <stdio.h>
#include "../FastFile.h"
#include "../vector.h"

int main(int argc, char **argv)
{
	void *buf = NULL;
	void *decompressed_buf;
	size_t size = 0;
	size_t decompressed_size = 0;
	u32 section_count = 0;

	FastFile ff;

	if (argc < 2 || !argv[1])
	{
		printf("FastFile not provided\n");
		return -1;
	}

	FILE *f = fopen(argv[1], "rb");

	if (!f)
	{
		printf("Could not open %s\n", argv[1]);
		return -1;
	}

	buf = FastFile_Alloc(f, &size);

	if (!size || !buf)
	{
		printf("Failed to allocate FastFile\n");
		return -2;
	}

	fclose(f);

	FastFile_GetHeader(buf, &ff);
	FastFile_LoadSections(buf, size, &ff, &section_count);

	decompressed_buf = FastFile_Decrypt(buf, &decompressed_size, ff.sections, section_count);
	FastFile_Free(buf, ff.sections, section_count);

	FILE *test = fopen("test.section", "wb+");
	if (!test)
	{
		printf("Failed to open %s\n", "test.section");
		return -3;
	}

	fwrite(decompressed_buf, decompressed_size, 1, test);
	fclose(test);

	free(decompressed_buf);

	vec2_t tmp;
	Vec2UnpackTexCoords((int)0x2EB63BFE, &tmp);

	printf("U: %0f V: %0f\n", tmp.u, tmp.v);

	return 0;
}
