#pragma once

#include "common.h"

#include <xmmintrin.h>
#include <emmintrin.h>
#include <immintrin.h>

typedef struct vec2_t
{
	union
	{
		__m64 xy;
		__m64 uv;
		struct
		{
			float x, y;
		};
		struct
		{
			float u, v;
		};
	};
} vec2_t;

typedef struct vec3_t
{
	union
	{
		struct
		{
			float x, y, z;
		};
		struct
		{
			float r, g, b;
		};
	};
} vec3_t;

typedef struct vec4_t
{
	union
	{
		__m128 v;
		struct
		{
			float x, y, z, w;
		};
		struct
		{
			float r, g, b, a;
		};
	};
} vec4_t;

// Packed Vector Types

typedef union PackedTexCoords
{
	u32 packed;
	struct
	{
		u16 hw;
		u16 lw;
	};

} PackedTexCoords;

typedef union floatdwordpack_t
{
	float f;
	u32 dw;
	s32 sdw;
} floatdwordpack_t;

// Packed Vector Functions
EXPORT void Vec2UnpackTexCoords(int in, vec2_t *uv);